import { type } from 'os';
import React, { useReducer } from 'react'
import { UIContext, uiReducer } from './';

export interface UIState {
  sideMenuOpen: boolean;
  isAddingEntry: boolean;
  isDragging: boolean;
}

interface Props {
  children: React.ReactNode;
}

const UI_INITIAL_STATE: UIState = {
  sideMenuOpen: false,
  isAddingEntry: false,
  isDragging: false,
}

export const UIProvider: React.FC<Props> = ({ children }  ) => {
  const [state, dispatch] = useReducer(uiReducer, UI_INITIAL_STATE);

  const openSideMenu = () => dispatch(
    { type: 'UIOpenSideBar' }
  );

  const closeSideMenu = () => dispatch(
    { type: 'UICloseSideBar' }
  );

  const setIsAddingEntry = (isAdding: boolean) => dispatch(
    {type: 'UIToggleIsAddingEntry', payload: isAdding}
  );

  const setIsDragging = (isDragging: boolean) => dispatch(
    {type: 'UIToggleIsDragging', payload: isDragging}
  );

  return (
    <UIContext.Provider value={{
      ...state,
      openSideMenu,
      closeSideMenu,
      setIsAddingEntry,
      setIsDragging,
    }}>
      { children }
    </UIContext.Provider>
  )
}