import { UIState } from "./UIProvider";

type UIActionType = 
  | { type: 'UIOpenSideBar' }
  | { type: 'UICloseSideBar' }
  | { type: 'UIToggleIsAddingEntry', payload: boolean }
  | { type: 'UIToggleIsDragging', payload: boolean }

export const uiReducer = (state: UIState, action: UIActionType): UIState => {
  
   switch (action.type) {
    case 'UIOpenSideBar':
      return {
        ...state,
        sideMenuOpen: true,
      }
    
    case 'UICloseSideBar':
      return {
        ...state,
        sideMenuOpen: false,
      }
  
    case 'UIToggleIsAddingEntry':
      return {
        ...state,
        isAddingEntry: action.payload,
      }
  
    case 'UIToggleIsDragging':
      return {
        ...state,
        isDragging: action.payload,
      }
  
    default:
      return state;
   }
}