import { Entry, EntryStatus } from 'interfaces';
import React, { useEffect, useReducer } from 'react'
import { EntriesContext, entriesReducer } from './';
import { v4 as uuidv4 } from 'uuid';
import { entriesApi } from 'api';

export interface EntriesState {
  entries: Entry[];
}

interface Props {
  children: React.ReactNode;
}

const Entries_INITIAL_STATE: EntriesState = {
  entries: [],
}

export const EntriesProvider: React.FC<Props> = ({ children }  ) => {
  const [state, dispatch] = useReducer(entriesReducer, Entries_INITIAL_STATE);

  const addNewEntry = async (description: string, status: EntryStatus) => {
    const { data } = await entriesApi.post<Entry>('/entries', { description })

    console.log(data)

    dispatch({type: 'addEntry', payload: data});
  }

  const updateEntry = async ({ _id, description, status }: Entry) => {
    const entryToUpdate = {
      description,
      status,
    }

    try {
      const { data } = await entriesApi.put<Entry>(`/entries/${_id}`, entryToUpdate)

      dispatch({type: 'updateEntry', payload: data});
    } catch (err) {
      console.error(err)
    }
  }

  const refreshEntries = async () => {
    const { data } = await entriesApi.get<Entry[]>('/entries')
    dispatch({ type: 'refreshData', payload: data })
  }

  useEffect(() => {
    refreshEntries();
  }, []);

  return (
    <EntriesContext.Provider value={{
      ...state,
      addNewEntry,
      updateEntry,
    }}>
      { children }
    </EntriesContext.Provider>
  )
}