import { Entry } from 'interfaces';
import { EntriesState } from './EntriesProvider';

type EntriesActionType = 
  | { type: 'addEntry', payload: Entry }
  | { type: 'updateEntry', payload: Entry }
  | { type: 'refreshData', payload: Entry[] }

export const entriesReducer = (state: EntriesState, action: EntriesActionType): EntriesState => {

  switch (action.type) {
    case 'addEntry':
      return {
        ...state,
        entries: [...state.entries, action.payload],
      }

    case 'updateEntry':
      return {
        ...state,
        entries: state.entries.map(entry => {
          if (entry._id === action.payload._id) {
            return action.payload;
          }
          return entry;
        }),
      }
    
    case 'refreshData':
      return {
        ...state,
        entries: [...action.payload],
      }

    default:
      return state;
  }
}