import { db } from 'db'
import { Entry, IEntry } from 'models'
import mongoose from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = 
  | {  message: any }
  | IEntry

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  
  const { _id } = req.query;

  if ( !mongoose.isValidObjectId(_id)) {
    return res.status(400).json({message: 'invalid _id'})
  }

  switch ( req.method ) {
    case 'PUT':
      return updateEntry(req, res);
    case 'GET':
      return getEntryById(req, res);
    default:
      return res.status(400).json({ message: 'Endpoint no exist' })
  }
}

const getEntryById = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  const { _id } = req.query;
  await db.connect();

  const foundEntry = await Entry.findById(_id);

  await db.disconnect();

  if (!foundEntry) {
    return res.status(400).json({ message: 'Entry not found' })
  }
  
  return res.status(200).json(foundEntry);
}

const updateEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  const { _id } = req.query;

  await db.connect();
  
  const entryToUpdate = await Entry.findById(_id);

  if (!entryToUpdate) {
    await db.disconnect();
    return res.status(400).json({ message: 'Entry not found' })
  }

  const {
    description = entryToUpdate.description,
    status = entryToUpdate.status,
  } = req.body;

  try {
    const updatedEntry = await Entry.findByIdAndUpdate(
      _id, 
      { 
        description, 
        status,
      },
      {
        runValidators: true,
        new: true,
      }
    )
    await db.disconnect();
    return res.status(200).json(updatedEntry!);

  } catch (err: any) {
    await db.disconnect();
    return res.status(400).json({message: err.errors.status.message})

  }

}
