import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { Roboto } from '@next/font/google';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { darkTheme } from 'themes';
import { UIProvider } from 'context/ui';
import { EntriesProvider } from 'context/entries';

const roboto = Roboto({
  subsets: ['latin'],
  weight: ['400', '700'],
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <EntriesProvider>
      <UIProvider>
        <ThemeProvider theme={darkTheme}>
          <CssBaseline>
              <main className={roboto.className}>
                <Component {...pageProps} />
              </main>
          </CssBaseline>
        </ThemeProvider>
      </UIProvider>
    </EntriesProvider>
  );
}
