import styles from '@/styles/Home.module.css'
import { Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import { Layout } from 'components/layouts'
import { EntryList, NewEntry } from 'components/ui'
import { NextPage } from 'next'

const HomePage: NextPage = () => {
  return (
    <>
      <Layout title='Home'>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <Card sx={{height: 'calc(100vh - 150px)'}}>
              <CardHeader title="Pendientes" />

                {/* agregar entrada */}
                <NewEntry status={'pending'} />
                
                {/* lista */}
                <EntryList status={'pending'} />
            </Card>
          </Grid>

          <Grid item xs={12} md={4}>
            <Card sx={{height: 'calc(100vh - 150px)'}}>
              <CardHeader title="En Progreso" />
              {/* <NewEntry status={'inProgress'}/> */}
              
              <EntryList status={'inProgress'} />
            </Card>
          </Grid>

          <Grid item xs={12} md={4}>
            <Card sx={{height: 'calc(100vh - 150px)'}}>
              <CardHeader title="Completadas" />
              {/* <NewEntry status={'finished'}/> */}
              <EntryList status={'finished'} />
            </Card>
            <CardContent>
              
            </CardContent>
          </Grid>

        </Grid>
      </Layout>
    </>
  )
}

export default HomePage