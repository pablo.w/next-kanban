interface SeedData {
    entries: SeedEntry[];
}

interface SeedEntry {
    description: string,
    createdAt: number,
    status: string,
}

export const seedData: SeedData = {
    entries: [
        {
            description: 'card One pending',
            createdAt: Date.now(),
            status: 'pending',
        },
        {
            description: 'card Two inProgress',
            createdAt: Date.now(),
            status: 'inProgress',
        },
        {
            description: 'card Three finished',
            createdAt: Date.now(),
            status: 'finished',
        },
    ]
}