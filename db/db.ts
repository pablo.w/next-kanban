import mongoose from "mongoose";

/** 
 * 0 = Disconnected
 * 1 = Connected
 * 2 = Connecting
 * 3 = Disconnecting 
 * */

const mongoConnection = {
    isConnected: 0,
}

export const connect = async () => {
    if (mongoConnection.isConnected) {
        console.log('db already connected')
        return
    }

    if (mongoose.connections.length > 0) {
        mongoConnection.isConnected = mongoose.connection.readyState

        if (mongoConnection.isConnected === 1) {
            console.log('usando connection anteiror')
            return
        }

        await mongoose.disconnect();

    }

    await mongoose.connect(process.env.MONGODB_URL || '')

    mongoConnection.isConnected = 1;

    console.log('conectado a mongodb', process.env.MONGODB_URL)
}

export const disconnect = async () => {
    if (process.env.NODE_ENV === 'development') return
    
    if (mongoConnection.isConnected === 0) return

    await mongoose.disconnect();
    console.log('desconectado de mongodb')
}