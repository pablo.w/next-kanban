import { List, Paper } from '@mui/material'
import { EntriesContext } from 'context/entries';
import { UIContext } from 'context/ui';
import { EntryStatus } from 'interfaces'
import React, { DragEvent, useContext, useMemo } from 'react'
import { EntryCard } from './EntryCard'
import styles from './EntryList.module.css';

interface Props {
  status: EntryStatus;
}

export const EntryList: React.FC<Props> = ({status}) => {
  const { entries, updateEntry } = useContext(EntriesContext);
  const { isDragging, setIsDragging } = useContext(UIContext);

  const filteredByStatus = useMemo(() => (
    entries?.filter(entry => entry.status === status)
  ), [entries]);

  const handleDrop = (e: DragEvent) => {
    const id = e.dataTransfer.getData('id')
    const draggedEntry = entries.find(entry => entry._id === id)!;
    draggedEntry.status = status;
    updateEntry(draggedEntry);
    setIsDragging(false);
  }

  const allowDrop = (e: DragEvent) => {
    e.preventDefault();
  }

  return (
    <div 
      onDrop={handleDrop}
      onDragOver={ allowDrop }
      className={`${styles.entryList} ${isDragging ? styles.dragging : ''}`}
    >
      <Paper sx={{
        height: 'calc(100vh - 250px)',
        overflow: 'scroll',
        backgroundColor: 'transparent',
        padding: 1,
      }}>
        <List sx={{
          opacity: isDragging ? .5 : 1, transition: 'all ease .2s'
        }}>
          {filteredByStatus?.map(entry => (
            <EntryCard key={entry._id} entryData={entry} />
          ))}
        </List>
      </Paper>
    </div>
  )
}