import { Card, CardActionArea, CardActions, CardContent, Typography } from '@mui/material'
import { UIContext } from 'context/ui';
import { Entry } from 'interfaces'
import React, { DragEvent, useContext, useState } from 'react'

interface Props {
  entryData: Entry;
}

export const EntryCard: React.FC<Props> = ({entryData}) => {
  const { setIsDragging } = useContext(UIContext);
  const [draggingCard, setDraggingCard] = useState<boolean>(false);
  const createdMins = Math.round(entryData.createdAt / 1000 / 60 / 60);

  const handleDragStart = (e: DragEvent) => {
    e.dataTransfer.setData('id', entryData._id)
    setIsDragging(true);

  }

  const handleDragEnd = (e: DragEvent) => {
    setIsDragging(false);
  }

  return (
    <Card 
      sx={{
        marginBottom: 1 
      }}
      draggable
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
    >
      <CardActionArea>
        <CardContent>
          <Typography sx={{
            whiteSpace: 'pre-line'
          }}>
            {entryData.description}
          </Typography>
        </CardContent>
        <CardActions sx={{display: 'flex', justifyContent: 'flex-end', paddingRight: 2}}>
          <Typography variant='body2'>
            hace {entryData.createdAt} mins
          </Typography>
        </CardActions>
      </CardActionArea>

    </Card>
  )
}