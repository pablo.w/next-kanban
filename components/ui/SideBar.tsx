import { Box, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, Typography } from '@mui/material'
import React, { useContext, useState } from 'react'
import LabelSharpIcon from '@mui/icons-material/LabelSharp';
import { UIContext } from 'context/ui';

const menuItems = [
	'Inbox',
	'Starred',
	'Send Email',
	'Drafts',
];

export const SideBar = () => {
	const { sideMenuOpen, closeSideMenu } = useContext(UIContext);

	return (
		<Drawer
			anchor='left'
			open={sideMenuOpen}
			onClose={closeSideMenu}
		>
			<Box width={250}>
				<Box sx={{ padding: '5px 10px' }}>
					<Typography variant='h4'>Menu</Typography>

				</Box>
				<List>
					{menuItems.map((item, i): JSX.Element => (
						<ListItem button key={i}>
							<ListItemIcon>
								<LabelSharpIcon />
							</ListItemIcon>
							<ListItemText primary={item} />
						</ListItem>
					))}
				</List>

				<Divider />

				<List>
					{menuItems.map((item, i): JSX.Element => (
						<ListItem button key={i}>
							<ListItemIcon>
								<LabelSharpIcon />
							</ListItemIcon>
							<ListItemText primary={item} />
						</ListItem>
					))}
				</List>
			</Box>
		</Drawer>
	)
}