import { Box, Button, TextField } from '@mui/material'
import React, { ChangeEvent, useContext, useState } from 'react'
import DataSaverOnSharpIcon from '@mui/icons-material/DataSaverOnSharp';
import HighlightOffSharpIcon from '@mui/icons-material/HighlightOffSharp';
import AddToPhotosSharpIcon from '@mui/icons-material/AddToPhotosSharp';
import { EntryStatus } from 'interfaces';
import { EntriesContext } from 'context/entries';
import { UIContext } from 'context/ui';

interface Props {
  status: EntryStatus;
}

export const NewEntry: React.FC<Props> = ({status}) => {
  const [description, setDescription] = useState('');
  const [touched, setTouched] = useState(false);

  const { addNewEntry } = useContext(EntriesContext);
  const { isAddingEntry, setIsAddingEntry } = useContext(UIContext);

  const toggleState = () => setIsAddingEntry(!isAddingEntry);

  const touch = () => setTouched(true);

  const reset = () => {
    setTouched(false);
    toggleState();
    setDescription('');
  }

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setDescription(e.target.value);
  }

  const isError = description.length <= 0 && touched;

  const handleSave = () => {
    if (!description) return;

    //update context
    addNewEntry(description, status);

    reset();
  }

  return (
    <Box 
      sx={{
        marginBottom: 2,
        paddingX: 2,
      }}
    >

      {!isAddingEntry ? (
        <Button
          color='secondary'
          startIcon={<AddToPhotosSharpIcon />}
          variant='outlined'
          fullWidth
          onClick={toggleState}
        >
          Agregar Entrada
        </Button>
      ) : (
        <Box>
          <TextField 
            fullWidth 
            sx={{marginTop: 2, marginBottom: 1}}
            autoFocus
            multiline 
            label={'Nueva Entrada'}
            helperText={''}
            value={description}
            onChange={handleInputChange}
            error={isError}
            onBlur={touch}
          />

          <Box display={'flex'} justifyContent={'space-between'}>
            <Button 
              variant='text' 
              color='warning'
              endIcon={<HighlightOffSharpIcon />}
              onClick={reset}
            >
              Cancelar
            </Button>
            <Button 
              variant='outlined' 
              color='secondary'
              endIcon={<DataSaverOnSharpIcon />}
              onClick={handleSave}
            >
              Guardar
            </Button>
          </Box>
        </Box>
      )}

    </Box>
  )
}