import { AppBar, IconButton, Toolbar, Typography } from '@mui/material'
import React, { useContext } from 'react'
import WidgetsSharpIcon from '@mui/icons-material/WidgetsSharp';
import { UIContext } from 'context/ui';

export const NavBar = () => {
  const { openSideMenu } = useContext(UIContext);

  return (
    <AppBar position='sticky'>
        <Toolbar>
            <IconButton size='large' edge='start' onClick={openSideMenu}>
                <WidgetsSharpIcon />
            </IconButton>

            <Typography variant='h6'>Tickets!</Typography>
        </Toolbar>
    </AppBar>
  )
}