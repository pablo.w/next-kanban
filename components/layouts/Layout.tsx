import { Box } from '@mui/material'
import { NavBar, SideBar } from 'components/ui'
import Head from 'next/head'
import React, { ReactNode } from 'react'

interface Props {
  title?: string,
  children?: ReactNode,
}

export const Layout: React.FC<Props> = ({ title = 'Default Title', children }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Head>
        <title>{title}</title>
      </Head>

      <NavBar />
      <SideBar />

      <Box sx={{ padding: '10px 20px' }}>
        {children}
      </Box>

    </Box>
  )
}